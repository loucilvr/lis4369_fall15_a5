##LIS4369 Fall 2015 A5 - Inheritance, Overloading and Overriding
##Loucil Rubio

###11/23/2015

###Inheritance: Product (base class), Book (derived class), Software (derived class)
![A5_Shot1.png](https://bitbucket.org/repo/apGgqX/images/1211292313-A5_Shot1.png)
![A5_Shot2.png](https://bitbucket.org/repo/apGgqX/images/3229753890-A5_Shot2.png)